package server

import (
	"github.com/librespot-org/librespot-golang/librespot/player"
)

type FutifyAudioFile struct {
	track     *Track
	audioFile *player.AudioFile
	data      *SeekableBuffer
	metadata  bool
}

func NewFutifyAudioFile(track *Track, audioFile *player.AudioFile) *FutifyAudioFile {
	a := &FutifyAudioFile{track: track, audioFile: audioFile, data: NewSeekableBuffer(), metadata: false}
	return a
}

// func (file *FutifyAudioFile) start() error {
// 	t := file.track
// 	p := fmt.Sprintf("appsrc name=mysource is-live=true block=true ! oggdemux ! vorbisdec ! taginject tags=\"title=\\\"%s\\\",artist=\\\"%s\\\",album=\\\"%s\\\",genre=\\\"%s\\\"\" ! vorbisenc ! oggmux ! appsink name=appsink", t.Name, t.Artist, t.Album, t.Genre)
// 	pipeline, err := gst.ParseLaunch(p)
// 	if err != nil {
// 		fmt.Println("pipeline error", err)
// 		return err

// 	}

// 	element := pipeline.GetByName("mysource")
// 	elementPull := pipeline.GetByName("appsink")
// 	pipeline.SetState(gst.StatePlaying)
// 	totalRead := 0
// 	go func() {
// 		buffer := make([]byte, 3000)
// 		for {
// 			n, err := file.audioFile.Read(buffer)
// 			if n > 0 {
// 				totalRead += n
// 				err = element.PushBuffer(buffer[:n])
// 				if err != nil {
// 					fmt.Println("push buffer error")
// 				}
// 				//fmt.Println(fmt.Sprintf("Read %d data on %d", totalRead, audioFile.Size()))
// 			}
// 			if err != nil {
// 				if err != io.EOF {
// 					fmt.Println(fmt.Sprintf("MyReader err %s", err.Error()))
// 					pipeline.EndOfStream()
// 					return
// 				} else {
// 					fmt.Println("MyReader finish")
// 				}
// 				return
// 			} else if n == 0 {
// 				fmt.Println("sleep during retreive from spotify")
// 				time.Sleep(100 * time.Millisecond)
// 			}
// 		}
// 	}()

// 	go func() {
// 		totalSended := 0
// 		for {
// 			sample, err := elementPull.PullSample()
// 			if err != nil {
// 				if elementPull.IsEOS() == true {
// 					fmt.Println("eos")
// 					break
// 				} else {
// 					fmt.Println("GStream error", err)
// 					continue
// 				}
// 			}
// 			totalSended += len(sample.Data)
// 			file.data.Write(sample.Data)
// 		}
// 	}()

// 	return nil
// }

// func (file *FutifyAudioFile) start2() error {
// 	p := fmt.Sprintf("appsrc name=mysource is-live=true block=true ! appsink name=appsink")
// 	pipeline, err := gst.ParseLaunch(p)
// 	if err != nil {
// 		fmt.Println("pipeline error", err)
// 		return err

// 	}

// 	element := pipeline.GetByName("mysource")
// 	elementPull := pipeline.GetByName("appsink")
// 	pipeline.SetState(gst.StatePlaying)
// 	totalRead := 0
// 	go func() {
// 		buffer := make([]byte, 3000)
// 		for {
// 			n, err := file.audioFile.Read(buffer)
// 			if n > 0 {
// 				totalRead += n
// 				err = element.PushBuffer(buffer[:n])
// 				if err != nil {
// 					fmt.Println("push buffer error")
// 				}
// 				//fmt.Println(fmt.Sprintf("Read %d data on %d", totalRead, audioFile.Size()))
// 			}
// 			if err != nil {
// 				if err != io.EOF {
// 					fmt.Println(fmt.Sprintf("MyReader err %s", err.Error()))
// 					pipeline.EndOfStream()
// 					return
// 				} else {
// 					fmt.Println("MyReader finish")
// 				}
// 				return
// 			} else if n == 0 {
// 				fmt.Println("sleep during retreive from spotify")
// 				time.Sleep(100 * time.Millisecond)
// 			}
// 		}
// 	}()

// 	go func() {
// 		totalSended := 0
// 		for {
// 			sample, err := elementPull.PullSample()
// 			if err != nil {
// 				if elementPull.IsEOS() == true {
// 					fmt.Println("eos")
// 					break
// 				} else {
// 					fmt.Println("GStream error", err)
// 					continue
// 				}
// 			}
// 			totalSended += len(sample.Data)
// 			file.data.Write(sample.Data)
// 		}
// 	}()

// 	return nil
// }

// func (file *FutifyAudioFile) start3() error {
// 	go func() {
// 		buffer := make([]byte, 3000)
// 		for {
// 			n, err := file.audioFile.Read(buffer)
// 			if n > 0 {
// 				_, err = file.data.Write(buffer[:n])
// 				if err != nil {
// 					fmt.Println("push buffer error")
// 				}
// 				//fmt.Println(fmt.Sprintf("Read %d data on %d", totalRead, audioFile.Size()))
// 			}
// 			if err != nil {
// 				if err != io.EOF {
// 					fmt.Println(fmt.Sprintf("MyReader err %s", err.Error()))
// 					return
// 				} else {
// 					fmt.Println("MyReader finish")
// 				}
// 				return
// 			} else if n == 0 {
// 				fmt.Println("sleep during retreive from spotify")
// 				time.Sleep(100 * time.Millisecond)
// 			}
// 		}
// 	}()

// 	return nil
// }

func (file *FutifyAudioFile) Read(p []byte) (int, error) {
	if file.metadata {
		return file.data.Read(p)
	}
	return file.audioFile.Read(p)
}

func (file *FutifyAudioFile) Seek(offset int64, whence int) (int64, error) {
	if file.metadata {
		return file.data.Seek(offset, whence)
	}
	return file.audioFile.Seek(offset, whence)
}
