/*
 * Copyright (C) 2020  Romain Maneschi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import Qt.labs.settings 1.0

import "pages"
import "services"
import "model"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'futify.frenchutouch'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Component {
        id: playlistTypeFactory
        PlaylistType {}
    }

    Component {
        id: albumTypeFactory
        AlbumType {}
    }

    AdaptivePageLayout {
        id: pageStack
        anchors.fill: parent

        layouts: [
            PageColumnsLayout {
                when: true;
                // Just a single column layout
                PageColumn {
                    fillWidth: true
                }

            }
        ]
    
        Login {
            id: loginPage
            visible: false
            onLogin: function(username, password) {
                error = spotSession.login(username, password);
                if (error === "") {
                    pageStack.primaryPage = homePage;
                    homePage.init();
                } else {
                    loginPage.error = error;
                }
            }
        }

        Home {
            id: homePage
            onNavigate: function(pageName, pageParams, o3, o4, o5) {
                if (pageName === "playlist") {
                    var p = spotSession.getPlaylist(spotSession.loadPlaylist(pageParams.uuid));
                    var newPlaylist = playlistTypeFactory.createObject(playlistPage, p);
                    homePage.pageStack.addPageToNextColumn(homePage, playlistPage, {playlist: newPlaylist});
                } else if(pageName === "search") {
                    homePage.pageStack.addPageToNextColumn(homePage, searchPage, pageParams);
                    searchPage.changes();
                } else if(pageName === "settings") {
                    homePage.pageStack.addPageToNextColumn(homePage, settingsPage, pageParams);
                    settingsPage.init();
                } else if(pageName === "about") {
                    homePage.pageStack.addPageToNextColumn(homePage, aboutPage, pageParams);
                } else {
                    console.log('page not known', pageName, pageParams, o3, o4, o5);
                }
            }
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onStartPlaylist: function(playlist) {
                var p = spotSession.getPlaylist(spotSession.loadPlaylist(playlist.uuid));
                var newPlaylist = playlistTypeFactory.createObject(homePage, p);
                audioPlayer.startPlaylist(newPlaylist)
            }
        }

        Playlist {
            id: playlistPage
            playlist: PlaylistType {}
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onAddToEndQueue: function(track) {
                audioPlayer.appendTrack(track);
            }
            onStartPlaylist: function(playlist) {
                audioPlayer.startPlaylist(playlist);
            }
        }

        Album {
            id: albumPage
            album: AlbumType {}
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onAddToEndQueue: function(track) {
                audioPlayer.appendTrack(track);
            }
            onStartAlbum: function(album) {
                audioPlayer.startAlbum(album);
            }
        }

        Search {
            id: searchPage
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onAddToEndQueue: function(track) {
                audioPlayer.appendTrack(track);
            }
            onSelectAlbum: function(album) {
                var a = spotSession.getAlbum(spotSession.loadAlbum(album.uuid));
                var newAlbum = albumTypeFactory.createObject(albumPage, a);
                searchPage.pageStack.addPageToNextColumn(searchPage, albumPage, {album: newAlbum});
            }
            onStartAlbum: function(album) {
                audioPlayer.startAlbum(album);
            }
            onSelectPlaylist: function(playlist) {
                var p = spotSession.getPlaylist(spotSession.loadPlaylist(playlist.uuid));
                var newPlaylist = playlistTypeFactory.createObject(playlistPage, p);
                searchPage.pageStack.addPageToNextColumn(searchPage, playlistPage, {playlist: newPlaylist});
            }
            onStartPlaylist: function(playlist) {
                audioPlayer.startPlaylist(playlist);
            }
        }

        Settings {
            id: settingsPage
            onLogout: {
                spotSession.logout()
                pageStack.primaryPage = loginPage;
            }
        }

        About {
            id: aboutPage
        }

        Component.onCompleted: {
            console.log("OnComplete")
            if (spotSession.settings.theme === 0) {
                theme.name = "Ubuntu.Components.Themes.SuruDark"
            } else if(spotSession.settings.theme === 1) {
                theme.name = "Ubuntu.Components.Themes.Ambiance"
            } else {
                theme.name = ""
            }
            if (spotSession.isLogged || spotSession.tryLogin()) {
                pageStack.primaryPage = homePage;
                homePage.init();
                console.log("OnComplete :: GoTo homePage")
            } else {
                pageStack.primaryPage = loginPage;
                console.log("OnComplete :: GoTo LoginPage")
            }
        }
    }

    UserMetricsHelper {
        player: audioPlayer
    }

    Player {
        id: audioPlayer
    }

    BottomEdge {
        id: bottomEdge
        height: parent.height - units.gu(20)
        preloadContent: true
        contentComponent: Queue {
            player: audioPlayer
            height: bottomEdge.height
        }
        onCommitCompleted: {
            bottomEdge.contentItem.refresh();
            bottomEdge.contentItem.focusCurrentTrack();
        }
    }

}
