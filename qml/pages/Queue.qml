import QtQuick 2.12
import Ubuntu.Components 1.3
import QtMultimedia 5.12

import "../components"


Page {

    property var player

    function refresh() {
        queue.model = spotSession.getAlbum('queue').size;
    }

    function focusCurrentTrack() {
        queue.currentIndex = player.playlist.currentIndex;
        queue.positionViewAtIndex(queue.currentIndex, ListView.Beginning);
    }

    header: PageHeader {
        id: bottomEdgeHeader
        title: i18n.tr("Queue") + " " + Math.min((player.playlist.currentIndex + 1), player.playlist.itemCount) + " / " + player.playlist.itemCount
        trailingActionBar.numberOfSlots: 4
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Next")
                iconName: "media-seek-forward"
                enabled: player.playlist.itemCount > 0 && player.playlist.currentIndex < player.playlist.itemCount - 1
                onTriggered: {
                    player.playlist.next();
                }
            },
            Action {
                text: i18n.tr("Play/Pause")
                iconName: player.playbackState == Audio.PlayingState ? "media-playback-pause" : "media-playback-start"
                enabled: player.playlist.itemCount > 0
                onTriggered: {
                    if (player.playbackState != Audio.PlayingState) {
                        player.play();
                    } else {
                        player.pause();
                    }
                }
            },
            Action {
                text: i18n.tr("Precedent")
                iconName: "media-seek-backward"
                enabled: player.playlist.itemCount > 0 && player.playlist.currentIndex > 0
                onTriggered: {
                    player.playlist.previous();
                }
            },
            Action {
                text: i18n.tr("Clear")
                iconName: "toolkit_input-clear"
                enabled: player.playlist.itemCount > 0
                onTriggered: {
                    player.clear();
                    refresh();
                }
            }
        ]
    }

    

    ListView {
        id: queue
        anchors.top: bottomEdgeHeader.bottom
        width: parent.width
        height: parent.height - bottomEdgeHeader.height
        clip: true
        model: spotSession.getAlbum('queue').size
        delegate: TrackListItem {
            id: trackListItem
            track: spotSession.getTrackByAlbum('queue', index)
            canDelete: true
            activeSimpleClick: true
            onPlayTrack: {
                player.playlist.currentIndex = index;
            }
            onEndQueue: {
                player.appendTrack(track)
            }
            onDeleteTrack: {
                if (player.playlist.currentIndex == index && player.playbackState == Audio.PlayingState) {
                    player.stop();
                }
                player.playlist.removeItem(index);
                spotSession.deleteQueue(index);
                refresh();
            }
        }
        currentIndex: player.playlist.currentIndex
        highlight:  Rectangle {
            width: queue.width
            height: units.gu(10)
            color: UbuntuColors.green
            y: queue.currentItem == null ? -1 : queue.currentItem.y
            Behavior on y {
                SpringAnimation {
                    spring: 3
                    damping: 0.2
                }
            }
        }
        highlightFollowsCurrentItem: false
        focus: true
    }

}