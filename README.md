# Futify

Native qml spotify client

# Develop

- cd \$GOPATH
- Git clone
- Dependencies of nannu-c/qml-go: apt-get install qtdeclarative5-dev qtbase5-private-dev qtdeclarative5-private-dev libqt5opengl5-dev
- Link qt to global: ln -s /usr/include/x86_64-linux-gnu/qt5/QtCore/<QT_VERSION>/QtCore /usr/include/
- Dependencies of futify: apt-get install libtagc0-dev
- go get .
- run on desktop: clickable desktop --dark
- run on device: rm -rf build && clickable clean-build && clickable install && clickable launch && clickable logs

## License

Copyright (C) 2020 Romain Maneschi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
